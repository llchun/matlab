function [area,cf]=circle(r)
area=2*pi*r^2;
cf=2*pi*r;
end